package com.retry.run;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.retry.annotation.EnableRetry;

import com.retry.http.RetryHttpClientService;

@SpringBootApplication
@ComponentScan("com.retry.http")
@EnableRetry
public class RetryApplication implements CommandLineRunner {

	@Autowired
	private RetryHttpClientService retryHttpClientService;

	public static void main(String[] args) {
		SpringApplication.run(RetryApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		retryHttpClientService.getData();

	}
}
